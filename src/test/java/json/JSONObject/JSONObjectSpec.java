package json.JSONObject;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

import static org.assertj.core.api.Assertions.assertThat;

class JSONObjectSpec {

    private static final String simpleJson = " { \"keyOne\": \"first\" , \"keyTwo\" : \"second\" } ";

    @Test
    void checkDeserializationFromString() throws JSONException {
        JSONObject jsonAsObject = new JSONObject(simpleJson);
        Iterator iterator = jsonAsObject.keys();
        int count = 0;
        while (iterator.hasNext()) {
            iterator.next();
            count++;
        }
        assertThat(count).isEqualTo(2);
    }

    @DisplayName("JSONObject.class не сохраняет форматирование исходной строки")
    @Test
    void jsonStringRepresentationIsEqualToOriginal() {
        JSONObject jsonAsObject = new JSONObject(simpleJson);
        String serializedJson = jsonAsObject.toString();
        assertThat(serializedJson).isNotEqualTo(simpleJson);
    }
}
