package json.json_simple;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.Test;

public class Spec {

    private static final String simpleJson = " { \"keyOne\": \"first\" , \"keyTwo\" : \"second\" } ";

    @Test
    void checkDeserializationFromString() throws ParseException {

        JSONParser parser = new JSONParser();

        JSONObject obj = (JSONObject) parser.parse(simpleJson);

        System.out.println(obj.toString());
        System.out.println(obj.toJSONString());

//        assertThat(count).isEqualTo(2);
    }

}
